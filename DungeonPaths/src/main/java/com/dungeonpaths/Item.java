package com.dungeonpaths;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by fourn_000 on 06/07/13.
 */
public class Item {
    private int id;
    private String name;
    private String description;
    private int cost;
    private String image;

    public Item(int _id, String _name, String description, String image) {
        this.id   = _id;
        this.name = _name;
        this.description = description;
        this.cost = 0;
        this.image = image;
    }

    public Item(int _id, String _name, String description, int _cost, String image) {
        this.id   = _id;
        this.name = _name;
        this.description = description;
        this.cost = _cost;
        this.image = image;
    }

    public Item() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }
}
