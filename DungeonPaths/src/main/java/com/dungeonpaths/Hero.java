package com.dungeonpaths;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Random;

/**
 * Created by gregory on 06/07/13.
 */
public class Hero {
    public Random rand = new Random();

    private String name;
    private int hclass;
    private int level;
    private int xp;

    private int max_xp;
    private int firewall;
    private int viruses;
    private int code;
    private int intelligence;
    private int system;

    private Inventory invent;

    public Hero(String name, int hclass, int level, int xp, int firewall, int viruses, int code, int intelligence, int system) {
        this.name = name;
        this.hclass = hclass;
        this.level = level;
        this.xp = xp;
        this.firewall = firewall;
        this.viruses = viruses;
        this.code = code;
        this.intelligence = intelligence;
        this.system = system;

        this.invent = new Inventory();
    }

    public int inventoryCount() {
        return invent.count_item;
    }

    public void removeItemFromInventory(String _t) {
        invent.removeItem(_t);
    }

    public void addItemToInventory(Item _item) {
        invent.addItem(_item);
    }

    public Item getItemFromInventory(int _i) {
        return invent.getItem(_i);
    }

    public void addCoinToInventory(int _c) {
        invent.addCoin(_c);
    }

    public int getCoinFromInventory() {
        return invent.getCoin();
    }

    public void minusCoinInInventory(int _c) {
        invent.addCoin(-_c);
    }

    public Hero(String _name, int _class) {
        setName(_name);
        setClass(_class);

        level = 1; xp = 0;

        invent = new Inventory();
        setOtherStats(_class);
    }

    public String getStringClass() {
        String res = "";
        switch(getHClass()) {
            case 0:
                res = "Hacker"; break;
            case 1:
                res = "CEO";    break;
            case 2:
                res = "Coder";  break;
        }
        return res;
    }

    private void setOtherStats(int aClass) {
        switch(aClass) {
            case 0:
                firewall = 1;
                viruses = rand.nextInt(1) + 4;
                code = rand.nextInt(1) + 3;
                system = rand.nextInt(20) + 20;
                intelligence = rand.nextInt(2) + 1;
                break;
            case 1:
                firewall = rand.nextInt(2) + 1;
                viruses = rand.nextInt(1) + 3;
                code = rand.nextInt(1) + 2;
                system = rand.nextInt(20) + 30;
                intelligence = rand.nextInt(2) + 4;
                break;
            case 2:
                firewall = rand.nextInt(1) + 3;
                viruses = rand.nextInt(1) + 3;
                code = rand.nextInt(1) + 4;
                system = rand.nextInt(20) + 20;
                intelligence = rand.nextInt(2) + 2;
                break;
        }
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClass(int aClass) {
        this.hclass = aClass;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public void addXp(int _d) { this.xp += _d; }

    public int getFirewall() {
        return firewall;
    }

    public void setFirewall(int firewall) {
        this.firewall = firewall;
    }

    public int getViruses() {
        return viruses;
    }

    public void setViruses(int viruses) {
        this.viruses = viruses;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getSystem() {
        return system;
    }

    public void setSystem(int system) {
        this.system = system;
    }

    public void addSystem(int _d) { this.system += _d; }

    public int getHClass() {
        return hclass;
    }

    public void setMax_xp(int max_xp) {
        this.max_xp = max_xp;
    }

    public int getMax_xp() {
        return max_xp;
    }

    public int getCoins() {
        return invent.getCoin();
    }

    public void addCoins(int _d) {
        invent.addCoin(_d);
    }

}
