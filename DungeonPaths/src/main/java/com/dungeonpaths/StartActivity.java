package com.dungeonpaths;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by gregory on 2/27/16.
 */
public class StartActivity  extends Activity {

    Button newGame, loadButton, buttonHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.start_activity);
        getActionBar().hide();
        newGame = (Button) findViewById(R.id.newGame);
        loadButton = (Button)findViewById(R.id.loadButton);

        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChooseActivity.class);
                startActivity(intent);
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);

                if(!shared.getString("name","secret_name").equals("secret_name")) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("START", "load");

                    startActivity(intent);
                } else {
                    Toast.makeText(StartActivity.this, "Nothing to load.", Toast.LENGTH_LONG).show();
                }

            }
        });





    }
}

