package com.dungeonpaths;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gregory on 2/27/16.
 */
public class ChooseActivity extends Activity {

    List<Integer> ids = new ArrayList<Integer>();
    List<String> names = new ArrayList<String>();
    List<String> descriptions = new ArrayList<String>();
    List<String> images = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.choose_activity);
        getActionBar().hide();

        Spinner mySpinner = (Spinner)findViewById(R.id.spinner);

        try {
            int result = readXMLFile();
            String[] stockArr = new String[names.size()];
            stockArr = names.toArray(stockArr);
            String[] stockArr2 = new String[images.size()];
            stockArr2 = images.toArray(stockArr2);
            String[] stockArr3 = new String[descriptions.size()];
            stockArr3 = descriptions.toArray(stockArr3);


            mySpinner.setAdapter(new MyAdapter(ChooseActivity.this, R.layout.hero_choose_row, stockArr, stockArr2, stockArr3));
        } catch(Exception e) {

        }

        EditText heroName = (EditText) findViewById(R.id.hero_name);
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditText heroName = (EditText) findViewById(R.id.hero_name);
                Button createButton = (Button) findViewById(R.id.btn_hero);
                if (heroName.getText().toString().length() == 0) {
                    heroName.setError("Hero name is required!");
                    createButton.setEnabled(false);
                } else {
                    heroName.setError(null);
                    createButton.setEnabled(true);
                }
            }
        };
        heroName.addTextChangedListener(textWatcher);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.launch, menu);
        return true;
    }

    public void createHero(View view) {
        EditText text = (EditText) findViewById(R.id.hero_name);
        String heroName = text.getText().toString();
        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        int index = spinner.getSelectedItemPosition();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("START", "new");
        intent.putExtra("HERO_TYPE", index);
        intent.putExtra("HERO_NAME", heroName);

        startActivity(intent);
    }

    public int readXMLFile() {
        int pos = 0;

        List<String> name = new ArrayList<String>();
        String elemtext="";
        String tagname="";

        try {

            XmlResourceParser xpp = getResources().getXml(R.xml.heroes);
            XmlPullParser parser = null;
            //xpp.next();
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                tagname = xpp.getName();
                if (eventType == XmlPullParser.TEXT) {

                    elemtext = xpp.getText();
                }

                if (eventType == XmlPullParser.END_TAG) {
                    if(tagname.equals("id")) {
                        ids.add(Integer.parseInt(elemtext));
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("name")) {
                        names.add(elemtext);
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("image")) {
                        images.add(elemtext);
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("description")) {
                        descriptions.add(elemtext);
                        Log.d(tagname,elemtext);
                    }
                }
                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {

        } catch (IOException e) {

        }

        return 0;
    }

    public class MyAdapter extends ArrayAdapter<String>{
        String[] names1;
        String[] descriptions1;
        int[] images1;

        public MyAdapter(Context context, int textViewResourceId, String[] objects, String[] obj2, String[] obj3) {
            super(context, textViewResourceId, objects);
            names1 = objects;
            descriptions1 = obj3;
            images1 = new int[objects.length];
            for(int i=0;i<objects.length;i++) {
                images1[i] = getResources().getIdentifier(obj2[i], "drawable", context.getPackageName());
            }
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getLayoutInflater();
            View row=inflater.inflate(R.layout.hero_choose_row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.company);
            label.setText(names1[position]);

            TextView sub=(TextView)row.findViewById(R.id.sub);
            sub.setText(descriptions1[position]);

            ImageView icon=(ImageView)row.findViewById(R.id.image);
            icon.setImageResource(images1[position]);

            return row;
        }
    }
}