package com.dungeonpaths;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gregory on 2/27/16.
 */
public class MainActivity extends Activity {
    public static final String DEFAULT    = "NONE";

    int EnemyHP = 0; int EnemyAttack = 0; int EnemyDefense = 0;
    int MAXEnemyHP = 0; int MAXEnemyAttack = 0; int MAXEnemyDefense = 0;
    int virusesMade = 0; int virusAttack = 0;
    String curTitle = "", curEnemy = "";
    boolean type_boss = false;

    boolean global_answer = false;
    int currentStoryId = 0;
    int room = 1;
    boolean explored = false;
    boolean saving = false;

    private int[] buttons_id = {R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6 };
    public Button[] buttons_all = new Button[buttons_id.length];
    private TextView textView;
    private ArrayList<Button> levelButtons;
    private ActionBar actionBar;
    Hero hero;

    List<Item> all_items = new ArrayList<Item>();
    List<StoryView> all_stories = new ArrayList<StoryView>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        textView = (TextView) findViewById(R.id.textView);
        for(int i=0;i<buttons_id.length;i++) {
            buttons_all[i] = (Button) findViewById(buttons_id[i]);
            buttons_all[i].setVisibility(View.INVISIBLE);
        }

        String type = (String) getIntent().getStringExtra("START");

        readItemXmlFile();

        if(type.equals("new")) {
            int choosed = (int) getIntent().getIntExtra("HERO_TYPE",0);
            String name = (String) getIntent().getStringExtra("HERO_NAME");
            Log.d("hey", String.valueOf(choosed) + " " + name);

            hero = new Hero(name,choosed);

            int rs = readStoryXmlFile();

            //Log.d("tibut",all_stories.get(0).buttonTitles.get(0));
            //Log.d("tttt",String.valueOf(all_stories.get(0).buttonGotos.get(0)));
            startStory();
        } else {
            // readSharedPreferences
            loadHero();
            //hero = new Hero("1",0);
        }

        //readXMLFile();
/*
        int levelId = (int) getIntent().getIntExtra("FILE_ID", 0);
        this.level = Level.getLevel(getResources(), levelId);
        actionBar = getActionBar();
        actionBar.setTitle(Global.getHero().getName() + " in '" + level.getName() +  "'");*/
        //showCurrentEvent();


    }

    private int readStoryXmlFile() {
        String elemtext="";
        String tagname="";

        String title="";
        int    id=0;
        String text="";
        int k=0; int m=0;

        String LOG_TAG = "xml"; String tmp = ""; String tmp2 = "";

        try {
            XmlPullParser xpp = prepareXpp();

            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                //List<String> buttonTitles = new ArrayList<String>();
                //List<Integer> buttonGotos = new ArrayList<Integer>();
                all_stories.add(new StoryView());

                switch (xpp.getEventType()) {
                    case XmlPullParser.START_DOCUMENT:
                        Log.d(LOG_TAG, "START_DOCUMENT");
                        break;
                    case XmlPullParser.START_TAG:
                        Log.d(LOG_TAG, "START_TAG: name = " + xpp.getName()
                                + ", depth = " + xpp.getDepth() + ", attrCount = "
                                + xpp.getAttributeCount());
                        tmp = "";
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {
                            tmp = tmp + xpp.getAttributeName(i) + " = "
                                    + xpp.getAttributeValue(i) + ", ";
                        }
                        if (!TextUtils.isEmpty(tmp))
                            Log.d(LOG_TAG, "Attributes: " + tmp);
                        break;
                    case XmlPullParser.END_TAG:
                        Log.d(LOG_TAG, "END_TAG: name = " + xpp.getName());
                        if(xpp.getName().equals("room_id")) {
                            all_stories.get(k).id = Integer.parseInt(tmp2);
                            //id = Integer.parseInt(tmp2);
                            Log.d("room_id",tmp2);
                        } else if (xpp.getName().equals("title")) {
                            all_stories.get(k).title = tmp2;
                            //title = tmp2;
                            Log.d("title",tmp2);
                        } else if (xpp.getName().equals("action_text")) {
                            all_stories.get(k).text = tmp2;
                            Log.d("action_text",tmp2);
                        } else if (xpp.getName().equals("button_title")) {
                            all_stories.get(k).addButtonTitle(tmp2);
                            //buttonTitles.add(tmp2);
                            Log.d("buttonTitle", tmp2);
                        } else if (xpp.getName().equals("goto")) {
                            all_stories.get(k).addbuttonGotos(Integer.parseInt(tmp2));
                            //buttonGotos.add(Integer.parseInt(tmp2));
                            Log.d("goto",tmp2);
                        } else if(xpp.getName().equals("room")) {
                            Log.d("FUCK",all_stories.get(k).text);
                            k += 1; m = 0;
                            Log.d("next", "NEXT ROOM");
                            //all_stories.add(new StoryView(title, id, text, buttonTitles, buttonGotos));
                            //Log.d("ALL_STORIES", all_stories.get(0).buttonTitles.get(0));
                            //buttonTitles.removeAll(buttonTitles); buttonGotos.removeAll(buttonGotos);
                            //Log.d("ALL_STORIES2", all_stories.get(0).buttonTitles.get(0));
                            //buttonTitles.clear(); buttonGotos.clear();
                        } else if(xpp.getName().equals("button")) {
                            m += 1;
                        }
                        break;
                    case XmlPullParser.TEXT:
                        Log.d(LOG_TAG, "text = " + xpp.getText());
                        tmp2 = xpp.getText();
                        break;

                    default:
                        break;
                }
                xpp.next();
            }
            Log.d(LOG_TAG, "END_DOCUMENT");

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return 0;
    }

    XmlPullParser prepareXpp() {
        return getResources().getXml(R.xml.rooms);
    }

    private void readItemXmlFile() {
        //Item[] result = new Item[9];

        String elemtext="";
        String tagname="";
        int id=0; String name=""; String image=""; String desc=""; int cost=0;

        try {

            XmlResourceParser xpp = getResources().getXml(R.xml.item_array);
            XmlPullParser parser = null;
            //xpp.next();
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                tagname = xpp.getName();
                if (eventType == XmlPullParser.TEXT) {

                    elemtext = xpp.getText();
                }
                if (eventType == XmlPullParser.END_TAG) {
                    if(tagname.equals("item_id")) {
                        id = Integer.parseInt(elemtext);
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("item_name")) {
                        name = elemtext;
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("item_description")) {
                        desc = elemtext;
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("item_cost")) {
                        cost = Integer.parseInt(elemtext);
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("item_image")) {
                        image = elemtext;
                        Log.d(tagname,elemtext);
                    } else if(tagname.equals("item")) {
                        all_items.add(new Item(id,name,desc,cost,image));
                    }
                }


                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {

        } catch (IOException e) {

        }

        //return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.menu_save:
                saveHero();
                return true;

            case R.id.menu_settings:
                Log.d("hello","hello");
                showStats();
                // search action
                return true;

        }
        return false;
    }

    private void saveHero() {
        if(saving) {
            MediaPlayer mp = MediaPlayer.create(this, R.raw.touch);
            mp.setLooping(false);
            mp.start();

            updateProfile("name", hero.getName());
            updateProfile("class", hero.getHClass());
            updateProfile("firewall", hero.getFirewall());
            updateProfile("viruses", hero.getViruses());
            updateProfile("code", hero.getCode());
            updateProfile("intelligence", hero.getIntelligence());
            updateProfile("system", hero.getSystem());
            updateProfile("level", hero.getLevel());
            updateProfile("xp", hero.getXp());
            updateProfile("coin", hero.getCoins());
            updateProfile("room", room);

            String buf="";
            for(int i=0;i<hero.inventoryCount();i++) {
                String new_item = hero.getItemFromInventory(i).getName();
                buf = buf + findIntInAll(new_item) + ",";
            }
            Log.d("inv",buf);
            updateProfile("invent", buf);
            //updateProfile("room",hero.getName());
            //updateProfile("coin",hero.getIgetName());
            //updateProfile("inventory",hero.getName());
        } else {
            say("You can save your hero only in room.");
        }
    }

    private void loadHero() {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);

        String name__ = shared.getString("name", DEFAULT);
        int class__ = Integer.parseInt(shared.getString("class", DEFAULT));
        int level__ = Integer.parseInt(shared.getString("level", DEFAULT));
        int xp__ = Integer.parseInt(shared.getString("xp", DEFAULT));
        int firewall__ = Integer.parseInt(shared.getString("firewall", DEFAULT));
        int viruses__ = Integer.parseInt(shared.getString("viruses", DEFAULT));
        int code__ = Integer.parseInt(shared.getString("code", DEFAULT));
        int intelligence__ = Integer.parseInt(shared.getString("intelligence", DEFAULT));
        int system__ = Integer.parseInt(shared.getString("system", DEFAULT));

        int coin__ = Integer.parseInt(shared.getString("coin", DEFAULT));
        int room__ = Integer.parseInt(shared.getString("room", DEFAULT));
        room = room__;

        hero = new Hero(name__,class__,level__,xp__,firewall__,viruses__,code__,intelligence__,system__);


        String buf__ = shared.getString("invent", DEFAULT);
        char one = 'c';

        for(int i=0;i<buf__.length();i++) {
            if((buf__.charAt(i)) ==  ',') {
                Log.d("LOAD",findItemInAll(Character.getNumericValue(one)).getName());
                hero.addItemToInventory(findItemInAll(Character.getNumericValue(one)));
            } else {
                one = buf__.charAt(i);
            }
        }

        mainLoop();
        //String name__ = shared.getString("name",DEFAULT);


    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }


    private void updateProfile(String key,String value) {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void updateProfile(String key,int value) {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(key, String.valueOf(value));
        editor.commit();
    }

    public void showStats() {
        MediaPlayer mp = MediaPlayer.create(this, R.raw.touch);
        mp.setLooping(false);
        mp.start();

        Intent intent2 = new Intent(MainActivity.this, StatsActivity.class);
        intent2.putExtra("Name",hero.getName());
        intent2.putExtra("Class",hero.getStringClass());
        intent2.putExtra("Firewall", hero.getFirewall());
        intent2.putExtra("Viruses",hero.getViruses());
        intent2.putExtra("Code",hero.getCode());
        intent2.putExtra("Intelligence",hero.getIntelligence());
        intent2.putExtra("System",hero.getSystem());
        intent2.putExtra("Level",hero.getLevel());
        intent2.putExtra("Xp", hero.getXp());
        intent2.putExtra("MaxXp", hero.getMax_xp());
        intent2.putExtra("Coin",hero.getCoins());

        startActivity(intent2);
    }
    public int showStorieItem(int _id) {
        StoryView cur = all_stories.get(_id);
        setTitle(cur.title);

        textView.setText(cur.text.replace("\\n", "\n"));

        Log.d("me11", cur.buttonTitles.get(0));
        for(int i=0;i<cur.buttonTitles.size();i++) {
            buttons_all[i].setVisibility(View.VISIBLE);
            buttons_all[i].setText(cur.buttonTitles.get(i));

            buttons_all[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonClick(v);

                }
            });
        }
        for(int i=cur.buttonTitles.size();i<buttons_all.length-cur.buttonTitles.size();i++) {
            buttons_all[i].setVisibility(View.INVISIBLE);
        }
        return 0;
    }

    public void startStory() {
        currentStoryId = all_stories.get(0).id;
        int result = showStorieItem(0);

        /*for(int i=0;i<1;i++) {
            Log.d("me11", all_stories.get(i).title);
            Log.d("me11", all_stories.get(i).text);
            Log.d("me11", all_stories.get(i).buttonTitles.get(0));
            Log.d("me11", all_stories.get(i).buttonTitles.get(1));
        }*/
        Log.d("meow1","meow");
    }

    public void buttonClick(View view) {
        int id = view.getId();

        switch(currentStoryId) {
            case 1:
                if(id == R.id.button1) {
                    Log.d("story111",String.valueOf(findStoryInAll(2)));
                    currentStoryId = 2;
                    int buf1 = findStoryInAll(currentStoryId);
                    showStorieItem(buf1);
                } else {
                    Log.d("story111",String.valueOf(findStoryInAll(3)));
                    currentStoryId = 3;
                    gameOver(all_stories.get(findStoryInAll(currentStoryId)).text);
                }
                break;

            case 2:
                if(id == R.id.button1) {
                    hero.setFirewall(hero.getFirewall() + 5);
                    hero.setViruses(hero.getViruses() + 3);
                    hero.setCode(hero.getCode() + 3);
                    hero.setSystem(hero.getSystem() + 50);

                    hero.addItemToInventory(findItemInAll("Tech Support"));
                    hero.addItemToInventory(findItemInAll("USB"));
                } else if(id == R.id.button2) {
                    hero.setFirewall(hero.getFirewall()+4);
                    hero.setViruses(hero.getViruses() + 1);
                    hero.setCode(hero.getCode() + 2);
                    hero.setSystem(hero.getSystem() + 20);
                    hero.setIntelligence(hero.getIntelligence() + 1);

                    hero.addItemToInventory(findItemInAll("Tech Support"));
                    hero.addItemToInventory(findItemInAll("USB"));
                    hero.addItemToInventory(findItemInAll("Trojan Virus"));
                    hero.addItemToInventory(findItemInAll("Anti-Virus"));
                    //hero.addItemToInventory(findItemInAll("USB Mouse"));
                } else if(id == R.id.button3) {
                    hero.setFirewall(hero.getFirewall() + 3);
                    hero.setViruses(hero.getViruses() + 1);
                    hero.setCode(hero.getCode() + 1);
                    hero.setSystem(hero.getSystem() + 10);

                    hero.addItemToInventory(findItemInAll("USB"));
                    hero.addItemToInventory(findItemInAll("Trojan Virus"));
                    hero.addItemToInventory(findItemInAll("Tech Support"));
                    //hero.addItemToInventory(findItemInAll("Great IDE"));
                    hero.addItemToInventory(findItemInAll("Anti-Virus"));
                    //hero.addItemToInventory(findItemInAll("USB Mouse"));
                }
                Log.d("story",String.valueOf(findStoryInAll(4)));
                currentStoryId=4;
                showStorieItem(findStoryInAll(4));
                break;

            case 3:

                break;

            case 4:
                Log.d("meow","gav11");
                if(id == R.id.button1) {
                    hero.addCoins(20);
                    say("You get 20 coins.");
                    mainLoop();
                    // go to mainLoop
                } else {
                    gameOver(all_stories.get(findStoryInAll(3)).text);
                    // go to gameover activity
                }
                break;
        }

    }

    public int findStoryInAll(int _d) {
        for(int i=0;i<all_stories.size();i++) {
            if(all_stories.get(i).id == _d) {
                return i;
            }
        }

        return -1;
    }

    public Item findItemInAll(String _str) {
        for(int i=0;i<all_items.size();i++) {
            if(all_items.get(i).getName().equals(_str)) {
                return all_items.get(i);
            }
        }

        return new Item();
    }

    public Item findItemInAll(int _d) {
        for(int i=0;i<all_items.size();i++) {
            if(all_items.get(i).getId() == _d) {
                return all_items.get(i);
            }
        }

        return new Item();
    }

    public int findIntInAll(String _str) {
        for(int i=0;i<all_items.size();i++) {
            if(all_items.get(i).getName().equals(_str)) {
                return i;
            }
        }

        return -1;
    }

    public void say(String _str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("BIMO Message");

        builder.setMessage(_str)
                .setCancelable(false)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void sayIntent(String _str,Intent _intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("BIMO Message");

        builder.setMessage(_str)
                .setCancelable(false)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean saychoose(String _str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("BIMO Message");

        builder.setMessage(_str)
                .setCancelable(false)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        global_answer = true;
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        global_answer = false;
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        return global_answer;
    }


    public void mainLoop() {
        watchRoom();
    }

    public void gameOver(String _str) {
        setTitle("Game Over");
        textView.setText(_str);
        for(int i=0;i<buttons_id.length;i++) {
            buttons_all[i].setVisibility(View.INVISIBLE);
        }
        buttons_all[0].setVisibility(View.VISIBLE);
        buttons_all[0].setText("Go to menu");

        buttons_all[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, StartActivity.class);
                startActivity(intent);
            }
        });
    }

    public void watchRoom() {
        saving = true;
        setTitle("Room " + String.valueOf(room));

        textView.setText("Be careful exploring the the rooms...");

        buttons_all[0].setText("Look around");
        buttons_all[1].setText("Talk to the Shady Dealer");
        buttons_all[2].setText("Next Room");
        for(int i=0;i<buttons_all.length;i++) { buttons_all[i].setVisibility(View.VISIBLE); buttons_all[i].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonRoomClick(v);
            }
        });}
        buttons_all[3].setVisibility(View.INVISIBLE);
        buttons_all[4].setVisibility(View.INVISIBLE);
        buttons_all[5].setVisibility(View.INVISIBLE);

        if(hero.getXp() == hero.getLevel() * 100) {
            say("You are almost ready to level up... Boss FIGHT!!");
            curTitle = "Dangerous Fight"; curEnemy = "Epic Boss";
            EnemyHP = (int)((hero.getXp()+1)*10);
            EnemyDefense = (int)((hero.getXp()+1));
            EnemyAttack = (int)((hero.getXp()+1));
            MAXEnemyHP = EnemyHP;
            MAXEnemyDefense = EnemyDefense;
            MAXEnemyAttack = EnemyAttack;

            type_boss = true;
            doBattle(curTitle,curEnemy,type_boss);
            // do battle BOOSS
        }

    }

    private void buttonRoomClick(View _v) {
        int id_ = _v.getId();
        switch(id_) {
            case R.id.button1:
                exploreRoom();
                break;
            case R.id.button2:
                merchant();
                break;
            case R.id.button3:
                explored = false;
                room += 1;
                watchRoom();
                break;
        }
    }

    private void exploreRoom() {
        if(!explored) {
            int rand = hero.rand.nextInt(100);
            if (rand <= 10) {
                String str = "You find a mysterious looking computer possibly filled with BitCoins. Open?";

                if (hero.rand.nextInt(100) >= 90) {
                    say(str+"\nThe chest was filled with malware that infects your system.");
                    hero.setSystem(0);
                    gameOver("You died... The villager morns your death.");
                } else if(hero.rand.nextInt(100) >= 80){
                    say("You find a load of 500 COINS!!");
                    hero.addCoins(500);
                } else {
                    say("You find 50 Coins! Not bad.");
                    hero.addCoins(50);
                }
                //watchRoom();
            } else if (rand >= 95) {
                say("You find pack with USB, Tech Support and Anti-Virus.");
                hero.addItemToInventory(findItemInAll("USB"));
                hero.addItemToInventory(findItemInAll("Tech Support"));
                hero.addItemToInventory(findItemInAll("Anti-Virus"));
                watchRoom();
            } else if (rand >= 70) {
                say("You find nothing.");
            }else {
                say("You encounter an enemy coder!.");
                EnemyHP = ((hero.getLevel()+2)*10);
                EnemyAttack = ((hero.getLevel()+2)*2);
                EnemyDefense = ((hero.getLevel()+2)*2);
                MAXEnemyAttack = EnemyAttack; MAXEnemyDefense = EnemyDefense; MAXEnemyHP=EnemyHP;
                curTitle = "Fight"; curEnemy = "Enemy Hacker";

                type_boss = false;

                hero.setSystem(5);
                doBattle(curTitle,curEnemy,type_boss);
            }

            explored = true;
        } else {
            say("You cannot explore room twice.");
        }
        //watchRoom();
    }

    private void merchant() {
        setTitle("Shady Dealer");

        textView.setText("Hello, buy something?!\nYou have "+hero.getCoins()+" coins.");

        buttons_all[0].setText("USB - 10 Coins");
        buttons_all[1].setText("Tech Support - 20 Coins");
        buttons_all[2].setText("Trojan Virus - 30 Coins");
        buttons_all[3].setText("Anti-Virus - 15 Coins");
        buttons_all[4].setText("Nothing, thanks!");

        for(int i=0;i<buttons_all.length;i++) { buttons_all[i].setVisibility(View.VISIBLE); buttons_all[i].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonMerchantClick(v);
            }
        });}

        buttons_all[5].setVisibility(View.INVISIBLE);
    }

    private void buttonMerchantClick(View _v) {
        Log.d("coin", String.valueOf(hero.getCoins()));
        int id_ = _v.getId();
        switch(id_) {
            case R.id.button1:
                if(hero.getCoins() > findItemInAll("USB").getCost()) {
                    Log.d("COST_USB",findItemInAll("USB").getDescription());
                    hero.addCoins(-findItemInAll("USB").getCost());
                    hero.addItemToInventory(findItemInAll("USB"));
                    say("USB was added to your inventory");
                    merchant();
                } else {
                    say("You dont have coins for this");
                }
                break;
            case R.id.button2:
                if(hero.getCoins() > findItemInAll("Tech Support").getCost()) {
                    hero.minusCoinInInventory(findItemInAll("Tech Support").getCost());
                    hero.addItemToInventory(findItemInAll("Tech Support"));
                    say("Tech Support was added to your inventory");
                    merchant();
                } else {
                    say("You dont have coins for this");
                }
                break;
            case R.id.button3:
                if(hero.getCoins() > findItemInAll("Trojan Virus").getCost()) {
                    hero.addItemToInventory(findItemInAll("Trojan Virus"));
                    hero.minusCoinInInventory(findItemInAll("Trojan Virus").getCost());
                    say("Trojan Virus was added to your inventory");
                    merchant();
                } else {
                    say("You dont have coins for this");
                }
                break;
            case R.id.button4:
                Log.d("COST_AV",String.valueOf(findItemInAll("Anti-Virus").getCost()));
                if(hero.getCoins() > findItemInAll("Anti-Virus").getCost()) {
                    hero.addItemToInventory(findItemInAll("Anti-Virus"));
                    hero.minusCoinInInventory(findItemInAll("Anti-Virus").getCost());
                    say("Anti-Virus was added to your inventory");
                    merchant();
                } else {
                    say("You dont have coins for this");
                }
                break;
            case R.id.button5:
                watchRoom();
                break;
        }
    }

    public void doBattle(String _title,String name,boolean _type) {
        saving = false;

        setTitle(_title);

        textView.setText("Enemy:\nSystem Health: " + EnemyHP + "\nVirus Damage: " + EnemyAttack + "\nEnemy Firewall: " + EnemyDefense + "\n\nViruses made: " + virusesMade + "\nVirus damage: " + virusAttack);

        if(EnemyHP <= 0) {
            if(_type) {
                hero.addCoins((int) (MAXEnemyHP / 2));
                hero.setLevel(hero.getLevel() + 1);
                hero.setSystem(hero.getSystem() + 50);
                hero.setXp(0);

                hero.setFirewall(hero.getFirewall() + hero.getLevel());
                hero.setViruses(hero.getViruses() + hero.getLevel());
                hero.setCode(hero.getCode() + hero.getLevel());
                hero.setSystem(hero.getSystem() + hero.getLevel());
                hero.setIntelligence(hero.getIntelligence() + hero.getLevel());

                say("You won the battle VERSUS BOSS!!!!\nYou got " + (int) (MAXEnemyHP / 2) + " Coins. And you get " + hero.getLevel() + " level.");
                watchRoom();
            } else {
                say("You won the battle!\nYou got " + (int) (MAXEnemyHP / 2) + " Coins. And " + (int) (MAXEnemyHP / 2) + " experience.");
                hero.addCoins((int) (MAXEnemyHP / 2));
                hero.addXp((int) (MAXEnemyHP / 2));
                if (hero.getXp() > 100) {
                    hero.setXp(100);
                }
                hero.setSystem(hero.getSystem() + 20);
                watchRoom();
            }
        }

        buttons_all[0].setText("Send out Virus.");
        buttons_all[1].setText("Build Virus.");
        buttons_all[2].setText("Improve system's firewall.");
        buttons_all[3].setText("Repair system.");
        buttons_all[4].setText("Use Item.");
        for(int i=0;i<5;i++) {
            buttons_all[i].setVisibility(View.VISIBLE);
            buttons_all[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inBattleClick(v);
                }
            });
        }
        buttons_all[5].setVisibility(View.INVISIBLE);

    }

    private void inBattleClick(View _v) {
        int id_ = _v.getId();
        switch(id_) {
            case R.id.button1:
                if(virusesMade > 0) {
                    if(virusAttack >= EnemyDefense *2) {
                        EnemyHP -= virusAttack;
                    } else if(virusAttack - EnemyDefense > 0){
                        EnemyHP -= (virusAttack - EnemyDefense);
                    } else {
                        EnemyHP -= 1;
                    }
                    virusesMade -= 1;
                    enemyDealDamage();
                } else {
                    say("You need to make a virus!");
                }
                doBattle(curTitle,curEnemy,type_boss);
                break;
            case R.id.button2:
                virusesMade += 1;
                if (virusAttack > 0) {
                    virusAttack = (int)(hero.getViruses() + hero.getCode()/2);
                    virusAttack += 1;
                } else {
                    virusAttack = (int)(hero.getViruses() + hero.getCode()/2);
                }
                enemyDealDamage();
                say("You build a virus.");
                doBattle(curTitle, curEnemy,type_boss);
                break;
            case R.id.button3:
                hero.setFirewall(hero.getFirewall() + 2);
                say("Defense improved!");
                enemyDealDamage();
                doBattle(curTitle, curEnemy,type_boss);
                break;
            case R.id.button4:
                hero.setSystem(hero.getSystem() + (int) (hero.getIntelligence() / 2 + hero.getCode() / 2));
                say("System improved!");
                enemyDealDamage();
                doBattle(curTitle,curEnemy,type_boss);
                break;
            case R.id.button5:
                showInventory();
                break;
        }

        if(hero.getSystem() <= 0) {
            gameOver("You died... The villager morns your death.");
        }
    }

    private void enemyDealDamage() {
        if(EnemyAttack >= hero.getFirewall() *2) {
            hero.setSystem(hero.getSystem() - EnemyAttack);
        } else if(EnemyAttack - hero.getFirewall() > 0){
            hero.setSystem(hero.getSystem() - (EnemyAttack - hero.getFirewall()));
        } else {
            hero.setSystem(hero.getSystem()-2);
        }

    }

    private void showInventory() {
        setTitle("Inventory");
        textView.setText("Choose item from your Inventory.");
        for(int i=0;i<hero.inventoryCount();i++) {
            buttons_all[i].setText(hero.getItemFromInventory(i).getName());
            buttons_all[i].setVisibility(View.VISIBLE);

            buttons_all[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id___ = v.getId();
                    int pos = 0;
                    if(id___ == R.id.button1) {
                        pos = 0;
                    } else if(id___ == R.id.button2) {
                        pos = 1;
                    } else if(id___ == R.id.button3) {
                        pos = 2;
                    } else if(id___ == R.id.button4) {
                        pos = 3;
                    } else if(id___ == R.id.button5) {
                        pos = 4;
                    } else if(id___ == R.id.button6) {
                        pos = 5;
                    }

                    String getItemName = hero.getItemFromInventory(pos).getName();
                    Log.d("ITEM", getItemName);
                    if(getItemName.equals("USB")) {
                        hero.setSystem(hero.getSystem() + 15);
                        say(hero.getItemFromInventory(pos).getDescription());
                        hero.removeItemFromInventory("USB");
                        doBattle(curTitle,curEnemy,type_boss);
                    } else if (getItemName.equals("Tech Support")) {
                        say(hero.getItemFromInventory(pos).getDescription());
                        curEnemy="";curTitle="";
                        hero.setSystem(hero.getSystem()+30);
                        hero.removeItemFromInventory("Tech Support");
                        watchRoom();
                    } else if (getItemName.equals("Trojan Virus")) {
                        virusAttack += 3; virusesMade +=2;
                        say(hero.getItemFromInventory(pos).getDescription());
                        hero.removeItemFromInventory("Trojan Virus");
                        doBattle(curTitle, curEnemy,type_boss);
                    } else if (getItemName.equals("Anti-Virus")) {
                        hero.setFirewall(hero.getFirewall() + 5);
                        say(hero.getItemFromInventory(pos).getDescription());
                        hero.removeItemFromInventory("Anti-Virus");
                        doBattle(curTitle,curEnemy,type_boss);
                    }

                }
            });
        }
        for(int i=hero.inventoryCount();i<buttons_all.length;i++) {
            buttons_all[i].setVisibility(View.INVISIBLE);
        }
    }
}
