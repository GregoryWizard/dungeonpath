package com.dungeonpaths;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 2/28/16.
 */
public class StoryView {
    public String title;
    public int    id;
    public String text;
    public List<String> buttonTitles;
    public List<Integer> buttonGotos;

    public StoryView(String title, int id, String text) {
        this.buttonTitles = new ArrayList<String>();
        this.buttonGotos = new ArrayList<Integer>();

        this.title = title;
        this.id = id;
        this.text = text;
    }

    public void addButtonTitle(String _t) {
        buttonTitles.add(_t);
    }

    public StoryView() {
        this.buttonTitles = new ArrayList<String>();
        this.buttonGotos = new ArrayList<Integer>();

        this.title = "null";
        this.id = -1;
        this.text = "null";
    }

    public void addbuttonGotos(int _t) {
        buttonGotos.add(_t);
    }


}
