package com.dungeonpaths;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

/**
 * Created by root on 2/28/16.
 */
public class StatsActivity  extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.stats);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        setTitle("Stats");

        getWindow().setLayout((int) (dm.widthPixels * .8), (int) (dm.heightPixels * .8));

        String Name      = (String) getIntent().getStringExtra("Name");
        String aClass    = (String) getIntent().getStringExtra("Class");
        int Firewall     = (int) getIntent().getIntExtra("Firewall", 0);
        int Viruses      = (int) getIntent().getIntExtra("Viruses", 0);
        int Code         = (int) getIntent().getIntExtra("Code", 0);
        int Intelligence = (int) getIntent().getIntExtra("Intelligence", 0);
        int System       = (int) getIntent().getIntExtra("System", 0);
        int Level        = (int) getIntent().getIntExtra("Level", 0);
        int Xp           = (int) getIntent().getIntExtra("Xp", 0);
        int MaxXp        = (int) getIntent().getIntExtra("MaxXp", 0);
        int Coin         = (int) getIntent().getIntExtra("Coin", 0);

        ((TextView)findViewById(R.id.name_stat)).setText(Name);
        ((TextView)findViewById(R.id.class_stat)).setText(aClass);
        ((TextView)findViewById(R.id.firewall_stat)).setText(String.valueOf(Firewall));
        ((TextView)findViewById(R.id.viruses_stat)).setText(String.valueOf(Viruses));
        ((TextView)findViewById(R.id.code_stat)).setText(String.valueOf(Code));
        ((TextView)findViewById(R.id.intelligence_stat)).setText(String.valueOf(Intelligence));
        ((TextView)findViewById(R.id.system_stat)).setText(String.valueOf(System));
        ((TextView)findViewById(R.id.level_stat)).setText(String.valueOf(Level));
        ((TextView)findViewById(R.id.xp_stat)).setText(String.valueOf(Xp));
        ((TextView)findViewById(R.id.coin_stat)).setText(String.valueOf(Coin));

    }

}
