package com.dungeonpaths;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 2/28/16.
 */
public class Inventory {
    protected int count_item;
    protected List<Item> inventory;
    protected int coin;
    protected int max = 5;

    public Inventory() {
        inventory  = new ArrayList<Item>();
        count_item = 0;
    }

    public void addItem(Item _item) {
        if(count_item+1 > 5) {

        } else {
            inventory.add(_item);
            count_item += 1;
        }
    }

    public void removeItem(String _t) {
        for(int i=0;i<inventory.size();i++) {
            if(inventory.get(i).getName().equals(_t)) {
                inventory.remove(i);
                count_item -= 1;
            }
        }
    }

    public Item getItem(int _pos) {
        return inventory.get(_pos);
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public void addCoin(int _d) { this.coin += _d; }

}
